import Image from 'next/image'
import { Inter } from 'next/font/google'
import UserInterface from '@/components/UserInterface';

const inter = Inter({ subsets: ['latin'] })

export default function Home() {
  return (
    <div>
      <UserInterface backendName="flask" />
    </div>
  );
}
